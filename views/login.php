<?php
/**
 * 3NInterface - Login page
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/

if (array_key_exists('action', $_GET) && $_GET['action'] === 'disconnect')
	$Message = 'Vous avez été déconnecté'
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Connexion</title>
	<link rel="stylesheet" type="text/css" href="/css/lib/bootstrap.min.css">
	<script src="/js/lib/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="/js/lib/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/login.js"></script>
</head>
<body>
<div id="errorMsgDiv" class="row form-group" style="display: none;">
	<div class="col-xs-6 col-xs-offset-3">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong><span class="msgContent"></span></strong>
		</div>
	</div>
</div>
<div id="successMsgDiv" class="row form-group" style="display: none;">
	<div class="col-xs-6 col-xs-offset-3">
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong><span class="msgContent"></span></strong>
		</div>
	</div>
</div>

<div id="loginContainer" class="container">
	<div class="row form-group">
		<div class="col-xs-2 col-xs-offset-3">
			<h2>Connexion</h2>
			<hr>
		</div>
	</div>
	<?php if (isset($Message)) { ?>
		<div class="row form-group alert-active">
			<div class="col-xs-6 col-xs-offset-3">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong><?php echo $Message; ?></strong>
				</div>
			</div>
		</div>
	<?php } ?>
	<div id="loginArea" class="row">
		<form id="loginForm">
			<div class="col-xs-6 col-xs-offset-3 well">
				<fieldset>
					<legend>Veuillez entrer vos information de connexion:</legend>
					<div class="row form-group">
						<label for="loginInput" class="col-xs-3 col-form-label">Identifiant</label>
						<div class="col-xs-7">
							<input type="text" id="loginInput" class="form-control" name="login" required>
						</div>
					</div>
					<div class="row form-group">
						<label for="passwordInput" class="col-xs-3 col-form-label">Mot de passe</label>
						<div class="col-xs-7">
							<input type="password" id="passwordInput" class="form-control" name="passwd" required>
						</div>
					</div>
					<br/>
					<div class="form-group">
						<div class="col-xs-5 col-xs-offset-9">
							<button type="submit" class="btn btn-default">Connexion</button>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	</div>
</div>
</body>
</html>

