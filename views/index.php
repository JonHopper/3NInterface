<?php
/**
 * 3NInterface - Index page
 *
 * PHP 7.0
 *
 * @author     Xel NDEMBI <ndembi_x@etna-alternance.net>
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @author     Cedric NOMENTSOA <noment_c@etna-alternance.net>
 * @version    1.0
 **/
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="loadtoken" content="<?php echo $_SESSION['token']; ?>">
	<title>SQLI-Interface</title>
	<link href="/css/lib/bootstrap.min.css" rel="stylesheet">
	<link href="/css/style.css" rel="stylesheet">
	<script src="/js/lib/jquery-3.1.1.min.js"></script>
	<script src="/js/lib/bootstrap.min.js"></script>
	<script src="/js/index.js"></script>
	<script src="/js/load.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse sidebar" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Barre de navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">SQLI</a><br>
			<small>interface</small>
			<br/>
		</div>
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active">
					<a href="#" id="Accueil" class="linkNav">Accueil<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a>
				</li>
				<li>
					<a href="#" id="RER" class="linkNav">Module RER<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-blackboard"></span></a>
				</li>
				<li>
					<a href="#" id="RoadView" class="linkNav">Module trafic routier<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-road"></span></a>
				</li>
				<li>
					<a href="#" id="Weather" class="linkNav">Module météo<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-globe"></span></a>
				</li>
				<li>
					<a href="#" id="APIConf" class="linkNav">Configuration API<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a>
				</li>
				<li>
					<a id="disconnect-link" href="#">Déconnexion<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-off"></span></a>
				</li>

			</ul>
		</div>
	</div>
</nav>
<div class="main" id="principal-id">
    <div class="page-header">
        <h1>Dashboard</h1>
    </div>
    <div class="row bloc-profil">
        <div class="col-md-2 col-md-offset-1 text-profil">
            <img src="/img/john_profil.jpg" alt="Profil de Jon" class="img-profil" />
            <h4>Jon</h4>
        </div>
        <div class="col-md-2 col-md-offset-2 text-profil">
            <img src="/img/cedric_profil.jpg" alt="Profil de Jon" class="img-profil" />
            <h4>Cédric</h4>
        </div>
        <div class="col-md-2 col-md-offset-2 text-profil">
            <img src="/img/xel_profil.jpg" alt="Profil de Jon" class="img-profil" />
            <h4>Xel</h4>
        </div>
    </div>
    <div class="bloc-profil">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Bienvenue sur notre Back Office</div>
                    <div class="panel-body">
                        De la part de Jon, Cédric et Xel, nous vous souhaitons la bienvenue
                        sur le Back Office de gestion de l'API REST.<br /><br />
                        Dans cette interface, vous pourrez gérer :
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">Deux lignes de RER</li>
                        <li class="list-group-item">Le trafic urbain</li>
                        <li class="list-group-item">La météo</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	<footer>

	</footer>
</div>
</body>
</html>
