/**
 * BackOffice - Component loader AJAJ script @Client
 *
 * JS - JQuery
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @author     Cedric NOMENTSOA <noment_c@etna-alternance.net>
 * @version    1.0
 **/

function loadComponent(component, callback) {
	$.ajax({
		type: 'POST',
		url: '/load',
		data: {
			action: 'load',
			component: component,
			token: $('meta[name=loadtoken]').attr('content')
		},
		dataType: 'html',
		error: function (err) {
			err = JSON.parse(err.responseText);
			console.error(err.error + ' - ' + err.message);
		},
		success: function (content) {
			callback(component, content);
		}
	})
}