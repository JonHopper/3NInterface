/**
 * BackOffice - Login AJAJ script @Client
 *
 * JS - JQuery
 *
 * @author     Jonathan S. Hopper <jonathans.hopper@yahoo.fr>
 * @version    1.0
 **/

$(document).ready(function () {
	$('#loginForm').submit(loginRequest);
});

function loginRequest(event) {
	event.preventDefault();

	var loginData = {};
	$.each($(this).serializeArray(), function (i, field) {
		loginData[field.name] = field.value;
	});

	$.ajax({
		type: 'POST',
		url: '/login',
		data: {
			action: 'login',
			args: loginData
		},
		dataType: 'json',
		success: loginAnswer
	})
}

function loginAnswer(response) {
	if (response.status) {
		showAlert('success', 'Connecté');

		setTimeout(function () {
			window.location = '/';
		}, 1000);
	}
	else {
		showAlert('error', 'Identifiant / Mot de passe invalide');
	}
}

function showAlert(type, message) {
	$('.alert-active').remove();

	var alertDiv;

	if (type == 'error')
		alertDiv = $('#errorMsgDiv').clone();
	else
		alertDiv = $('#successMsgDiv').clone();

	alertDiv.find('.msgContent').html(message);
	alertDiv.prop('id', '').addClass('alert-active').show();

	$('#loginArea').before(alertDiv);
}