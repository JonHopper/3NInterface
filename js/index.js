/**
 * BackOffice - Index AJAJ script @Client
 *
 * JS - JQuery
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @author     Cédric NOMENTSOA <noment_c@etna-alternance.net>
 * @version    1.0
 **/

$(document).ready(function () {
	$('#disconnect-link').on('click', disconnect);
    $('.linkNav').on('click', preloadComponent);
});

function preloadComponent(event) {
    event.preventDefault();
    
    loadComponent($(this).attr("id"), changeComponent);
}

function changeComponent(component, content) {
    $('#principal-id').html(content);
    $('li[class="active"]').removeClass('active');
    $('#'+component+'').parent().addClass('active');
}

function disconnect(event) {
	event.preventDefault();

	$.ajax({
		type: 'POST',
		url: '/login',
		data: {
			action: 'login',
			args: {disconnect: true}
		},
		dataType: 'text',
		success: function () {
			window.location = '/login/disconnect';
		}
	})
}