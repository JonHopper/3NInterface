/**
 * BackOffice - Index AJAJ script @Client
 *
 * JS - JQuery
 *
 * @author     Cédric NOMENTSOA <noment_c@etna-alternance.net>
 * @version    1.0
 **/

$.ajax( {
    type: 'GET',
    url: '/conf/RER.json',
    success: function (data) {
        $('#widget-name').val(data.Version.Fields.field[0].fieldValue);
        $('#ligne-1').val($(data.Version.Fields.field[1].fieldValue.xml).text());
        $('#ligne-2').val($(data.Version.Fields.field[2].fieldValue.xml).text());
        $('#latitude').val(data.Version.Fields.field[3].fieldValue.latitude);
        $('#longitude').val(data.Version.Fields.field[3].fieldValue.longitude);
        $('#addresse').val(data.Version.Fields.field[3].fieldValue.address);
        $('#start-time').val(data.Version.Fields.field[4].fieldValue);
        $('#end-time').val(data.Version.Fields.field[5].fieldValue);
        $('#api-key').val(data.Version.Fields.field[6].fieldValue);
    }
});

$(document).ready(function () {
    $('#updateWeather').on('click', function (event) {
        event.preventDefault();
        resetForm();
        
        if (checkForm()) { 
            $.ajax({
                url: '/conf/RER.json', 
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                    $('#conf').text(data.replace(/^(.)/g, '\n$1').replace(/("latitude": )([^,]*)/, '$1' + $('#latitude').val()).replace(/("longitude": )([^,]*)/, '$1' + $('#longitude').val()).replace(/("address": )([^,]*)/, '$1"' + $('#addresse').val() + '"').replace(/("id": 323[\w\W\s]*?"fieldValue": )"(.*)"/, '$1"' + $('#widget-name').val() + '"').replace(/57600/, $('#start-time').val()).replace(/79200/, $('#end-time').val()).replace(/("id": 329[\w\W\s]*?"fieldValue": )"(.*)"/, '$1"' + $('#api-key').val() + '"').replace(/<para>.*<\/para>/, '<para>' + $('#ligne-1').val() + '</para>').replace(/<p>.*<\/p>/, '<p>' + $('#ligne-1').val() + '</p>'));
                }
            });
        }
    });
});

function checkForm() {
    var a = true;
    $('.form-control').each( function () {
        if ($(this).attr('id') == 'longitude' || $(this).attr('id') == 'latitude') {
            if (!$(this).val().match(/^\d+\.\d+$/)) {
                $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Coordonnée invalide</span>');
                a = false;
            }
        }
        else if ($(this).attr('id') == 'start-time' || $(this).attr('id') == 'end-time') {
            if (!$(this).val().match(/^\d+$/)) {
                $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Heure invalide</span>');
                a = false;
            }
        }
        else if ($(this).attr('title') == 'f') {
            a = a;
        }
        else if ($(this).val().length == 0) {
            $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Champ incomplet</span>');
            a = false;
        }
    });
    return a;
}

function resetForm() {
    $('form span').remove();
    $('.has-error').removeClass('has-error has-feedback');
}