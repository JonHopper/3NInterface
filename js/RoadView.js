/**
 * BackOffice - Index AJAJ script @Client
 *
 * JS - JQuery
 *
 * @author     Cédric NOMENTSOA <noment_c@etna-alternance.net>
 * @version    1.0
 **/

$.ajax( {
    type: 'GET',
    url: '/conf/RoadView.json',
    success: function (data) {
        $('#widget-name').val(data.Version.Fields.field[0].fieldValue);
        $('#latitude').val(data.Version.Fields.field[1].fieldValue.latitude);
        $('#longitude').val(data.Version.Fields.field[1].fieldValue.longitude);
        $('#addresse').val(data.Version.Fields.field[1].fieldValue.address);
        $('#desc').val(data.Version.Fields.field[3].fieldValue);
        var a = 1;
        for (let i of data.Version.Fields.field[2].fieldValue.xml.match(/<para>.*?<\/para>/g)) {
            $('#dest'+a).val(i.replace(/<para>(.*)<\/para>/, '$1'));
            a++;
        }
    }
});

$(document).ready(function () {
    $('#updateRoadView').on('click', function (event) {
        event.preventDefault();
        resetForm();
        
        if (checkForm()) { 
            $.ajax({
                url: '/conf/RoadView.json', 
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                    data = data.replace(/^(.)/g, '\n$1').replace(/("latitude": )([^,]*)/, '$1' + $('#latitude').val()).replace(/("longitude": )([^,]*)/, '$1' + $('#longitude').val()).replace(/("address": )([^,]*)/, '$1"' + $('#addresse').val() + '"').replace(/("id": 337[\w\W\s]*?"fieldValue": )"(.*)"/, '$1"' + $('#widget-name').val() + '"').replace(/("id": 340[\w\W\s]*"fieldValue": )"(.*)"/, '$1"' + $('#desc').val() + '"').replace(/<para>vincennes<\/para><para>puteaux<\/para><para>ivry-sur-seine<\/para>/, '').replace(/<p>vincennes<\/p><p>puteaux<\/p><p>ivry-sur-seine<\/p>/, '');
                    for (var a = 1; a <= 10; a++) {
                        if ($('#dest'+a).val().length > 0) {
                            data = data.replace(/(xml.*?)<\/section>/, '$1<para>' + $('#dest'+a).val() + '</para><\/section>').replace(/(xhtml5edit.*)<\/section>/, '$1<p>' + $('#dest'+a).val() + '</p></section>');
                        }
                    }
                    $('#conf').text(data);
                }
            });
        }
    });
});

function checkForm() {
    var a = true;
    $('.form-control').each( function () {
        if ($(this).attr('id') == 'longitude' || $(this).attr('id') == 'latitude') {
                if (!$(this).val().match(/^\d+\.\d+$/)) {
                    $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Coordonnée invalide</span>');
                    a = false;
                }
            }
        else if ($(this).attr('title') == 'Destination') {
            a = a;
        }
        else if ($(this).val().length == 0) {
            $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Champ incomplet</span>');
            a = false;
        }
    });
    return a;
}

function resetForm() {
    $('form span').remove();
    $('.has-error').removeClass('has-error has-feedback');
}