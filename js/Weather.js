/**
 * BackOffice - Index AJAJ script @Client
 *
 * JS - JQuery
 *
 * @author     Cédric NOMENTSOA <noment_c@etna-alternance.net>
 * @version    1.0
 **/

$.ajax( {
    type: 'GET',
    url: '/conf/Weather.json',
    success: function (data) {
        $('#widget-name').val(data.Version.Fields.field[0].fieldValue);
        $('#latitude').val(data.Version.Fields.field[1].fieldValue.latitude);
        $('#longitude').val(data.Version.Fields.field[1].fieldValue.longitude);
        $('#addresse').val(data.Version.Fields.field[1].fieldValue.address);
        $('#desc').val($(data.Version.Fields.field[2].fieldValue.xml).text().replace(/\s/, ''));
    }
});

$(document).ready(function () {
    $('#updateWeather').on('click', function (event) {
        event.preventDefault();
        resetForm();
        
        if (checkForm()) { 
            $.ajax({
                url: '/conf/Weather.json', 
                type: 'GET',
                dataType: 'html',
                success: function (data) {
                    $('#conf').text(data.replace(/^(.)/g, '\n$1').replace(/("latitude": )([^,]*)/, '$1' + $('#latitude').val()).replace(/("longitude": )([^,]*)/, '$1' + $('#longitude').val()).replace(/("address": )([^,]*)/, '$1"' + $('#addresse').val() + '"').replace(/("id": 341[\w\W\s]*"fieldValue": )"(.*)"/, '$1"' + $('#widget-name').val() + '"').replace(/<para>.*<\/para>/, '<para>' + $('#desc').val() + '</para>').replace(/<p>.*<\/p>/, '<p>' + $('#desc').val() + '</p>'));
                }
            });
        }
    });
});

function checkForm() {
    var a = true;
    $('.form-control').each( function () {
        if ($(this).attr('id') == 'longitude' || $(this).attr('id') == 'latitude') {
                if (!$(this).val().match(/^\d+\.\d+$/)) {
                    $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Coordonnée invalide</span>');
                    a = false;
                }
            }
        else if ($(this).val().length == 0) {
            $(this).parent().addClass('has-error has-feedback').append('<span class="glyphicon glyphicon-warning-sign form-control-feedback"></span><span class="help-block">Champ incomplet</span>');
            a = false;
        }
    });
    return a;
}

function resetForm() {
    $('form span').remove();
    $('.has-error').removeClass('has-error has-feedback');
}