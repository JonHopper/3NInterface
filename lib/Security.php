<?php
/**
 * 3NInterface - Security Lib
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/

namespace TNInterface\Lib;

abstract class Security
{
	/**
	 * Calculate hash for password storage
	 *
	 * @param string $user Username
	 * @param string $passwd Clear-text password
	 *
	 * @return string
	 */
	public static function passwordHash(string $user, string $passwd): string
	{
		/* Check parameters */
		if (!count($user) or !count($passwd))
			return '';

		/* Compute composed password (unique from user) */
		$upasswd = hash('sha256', $user . $passwd);

		return password_hash($upasswd, PASSWORD_BCRYPT);
	}

	/**
	 * Check a password matches a hash
	 *
	 * @param string $user Username
	 * @param string $pass Clear-text password to be tested
	 * @param string $passwdHash Hashed password
	 *
	 * @return bool
	 */
	public static function passwordCheck(string $user, string $passwd, string $passwdHash): bool
	{
		/* Check parameters */
		if (!count($user) or !count($passwd) or !count($passwdHash))
			return false;

		/* Compute composed password (unique from user) */
		$upasswd = hash('sha256', $user . $passwd);

		return password_verify($upasswd, $passwdHash);
	}

	/**
	 * Generate an 12 hours session token for an user
	 *
	 * @param string $user Username
	 *
	 * @return string
	 */
	public static function sessionGenerateToken(string $user) : string
	{
		/* Check parameter */
		if (!count($user))
			return '';

		/* Compute token */
		return hash('sha256', date('y-m-j-a') . $user);
	}

	/**
	 * Check session token
	 *
	 * @return bool
	 */
	public static function sessionCheck(): bool
	{
		return (isset($_SESSION['user']) and isset($_SESSION['token']) and $_SESSION['token'] == self::sessionGenerateToken($_SESSION['user']));
	}
}