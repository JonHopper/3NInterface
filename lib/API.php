<?php
/**
 * 3NInterface - Security Lib
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/

namespace TNInterface\Lib;


abstract class API
{
	/** Relative path (from project root) of API configuration file
	 * (Will also be used as encryption IV)
	 * @type string
	 */
	private static $APICONF = 'APIConf.json';

	/** Secret key used for password encryption
	 * @type string
	 */
	private static $SECRET = 'Run, you clever boy !';

	/** Encryption method (for method list see: openssl_get_cipher_methods)
	 * @type string
	 */
	private static $E_METHOD = 'AES-256-CBC';

	/**
	 * Set API configuration
	 *
	 * @param string $URL API URL
	 * @param string $user Username
	 * @param string $passwd Clear-text password
	 *
	 * @return bool
	 */
	public static function set(string $URL, string $user, string $passwd): bool
	{
		/* Check parameters */
		if (!count($URL) or !count($user) or !count($passwd))
			return false;

		/* Encrypt password */
		$CryptCxt = self::getEContext();
		$EPassword = openssl_encrypt($passwd, self::$E_METHOD, $CryptCxt['KEY'], 0, $CryptCxt['IV']);
		if ($EPassword === false)
			return false;

		$APIConf = [
			'URL'      => $URL,
			'login'    => $user,
			'password' => base64_encode($EPassword)
		];

		/* Convert to JSON */
		$JSONAPIConf = json_encode($APIConf);

		/* Write back DB to file */
		return (file_put_contents(self::APIConf(), $JSONAPIConf, LOCK_EX) !== false);
	}

	/**
	 * Get API configuration
	 *
	 * @return array {      (Null in case of error)
	 * @type string        'URL', 'login' and 'password'
	 *      =>
	 * @type string         Values
	 *  }
	 */
	public static function get()
	{
		/* Read existing content */
		if (($fileDBContent = file_get_contents(self::APIConf())) === false)
			return null;

		/* Translate JSON (as associative array) */
		if (($APIConf = json_decode($fileDBContent, true)) === null)
			return null;

		/* Decrypt password */
		$CryptCxt = self::getEContext();
		$CPassword = openssl_decrypt(base64_decode($APIConf['password']), self::$E_METHOD, $CryptCxt['KEY'], 0, $CryptCxt['IV']);
		if ($CPassword === false)
		{
			$APIConf['password'] = null;
			return $APIConf;
		}

		$APIConf['password'] = $CPassword;
		return $APIConf;
	}

	/** Return an en/decryption context
	 *
	 * @return array {
	 * @type string         'KEY' and 'IV'
	 *      =>
	 * @type string         Values
	 * }
	 */
	private static function getEContext()
	{
		return [
			'KEY' => hash('sha256', self::$SECRET),
			'IV' => substr(hash('sha256', self::$APICONF), 0, 16)
		];
	}

	/**
	 * Return absolute path of API configuration file
	 *
	 * @return string
	 */
	private static function APIConf(): string
	{
		return dirname(__DIR__) . '/' . self::$APICONF;
	}
}