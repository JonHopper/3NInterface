<?php
/**
 * 3NInterface - User database Lib
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/

namespace TNInterface\Lib;

include_once 'Security.php';

abstract class UserDB
{
	/** Relative path (from project root) of user database file
	 * @type string
	 */
	private static $USERDB = 'userdb.json';

	/**
	 * Create an user
	 *
	 * @param string $user Username
	 * @param string $passwd Clear-text password
	 *
	 * @return bool
	 */
	public static function add(string $user, string $passwd): bool
	{
		/* Check parameters */
		if (!count($user) or !count($passwd))
			return false;

		$UserDBContent = [];

		/* Check if DB file already exists */
		if (file_exists(self::UserDB()))
		{
			/* Read existing content */
			if (($fileDBContent = file_get_contents(self::UserDB())) === false)
				return false;

			/* Translate JSON (as associative array) */
			if (($UserDBContent = json_decode($fileDBContent, true)) === null)
				return false;
		}

		/* Check user not already exists */
		if (array_key_exists($user, $UserDBContent))
			return false;

		/* Add password hash to DB content */
		$UserDBContent[$user] = Security::passwordHash($user, $passwd);

		/* Convert to JSON */
		$JSONUserDB = json_encode($UserDBContent);

		/* Write back DB to file */
		return (file_put_contents(self::$USERDB, $JSONUserDB, LOCK_EX) !== false);
	}

	/**
	 * Check password of an user
	 *
	 * @param string $user Username
	 * @param string $passwd Clear-text password to be tested
	 *
	 * @return bool
	 */
	public static function verify(string $user, string $passwd): bool
	{
		/* Check parameters */
		if (!count($user) or !count($passwd))
			return false;

		/* Check if DB file exists */
		if (!file_exists(self::UserDB()))
			return false;

		/* Read existing content */
		if (($fileDBContent = file_get_contents(self::UserDB())) === false)
			return false;

		/* Translate JSON (as associative array) */
		if (($UserDBContent = json_decode($fileDBContent, true)) === null)
			return false;

		/* Ensure user exists */
		if (!array_key_exists($user, $UserDBContent))
			return false;

		/* Check password hash */
		return Security::passwordCheck($user, $passwd, $UserDBContent[$user]);
	}

	/**
	 * Set password of an user
	 * (Same as passwordStore but without username checking)
	 *
	 * @param string $user Username
	 * @param string $passwd Clear-text password
	 *
	 * @return bool
	 */
	public static function  set(string $user, string $passwd): bool
	{
		/* Check parameters */
		if (!count($user) or !count($passwd))
			return false;

		$UserDBContent = [];

		/* Check if DB file already exists */
		if (file_exists(self::UserDB()))
		{
			/* Read existing content */
			if (($fileDBContent = file_get_contents(self::UserDB())) === false)
				return false;

			/* Translate JSON (as associative array) */
			if (($UserDBContent = json_decode($fileDBContent, true)) === null)
				return false;
		}

		/* Add password hash to DB content */
		$UserDBContent[$user] = Security::passwordHash($user, $passwd);

		/* Convert to JSON */
		$JSONUserDB = json_encode($UserDBContent);

		/* Write back DB to file */
		return (file_put_contents(self::UserDB(), $JSONUserDB, LOCK_EX) !== false);
	}

	/**
	 * Delete user
	 *
	 * @param string $user Username
	 *
	 * @return bool
	 */
	public static function delete(string $user): bool
	{
		/* Check parameter */
		if (!count($user))
			return false;

		/* Check if DB file exists */
		if (!file_exists(self::UserDB()))
			return false;

		/* Read existing content */
		if (($fileDBContent = file_get_contents(self::UserDB())) === false)
			return false;

		/* Translate JSON (as associative array) */
		if (($UserDBContent = json_decode($fileDBContent, true)) === null)
			return false;

		/* Check user exists */
		if (!array_key_exists($user, $UserDBContent))
			return true;

		/* Delete user */
		unset($UserDBContent[$user]);

		/* Convert to JSON */
		$JSONUserDB = json_encode($UserDBContent);

		/* Write back DB to file */
		return (file_put_contents(self::$USERDB, $JSONUserDB, LOCK_EX) !== false);
	}


	/**
	 * Return absolute path of user database file
	 *
	 * @return string
	 */
	private static function UserDB(): string
	{
		return dirname(__DIR__) . '/' . self::$USERDB;
	}
}