<?php
/**
 * 3NInterface - Login AJAJ script @Server
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/

include_once dirname(__DIR__) . '/lib/UserDB.php';

/* Check arguments */
if (!array_key_exists('args', $_POST))
{
	http_response_code(400);
	echo json_encode([
		                 'error'   => '400 Bad request',
		                 'message' => 'Argument(s) expected'
	                 ]);
	exit;
}


/* Connection request */
if (array_key_exists('login', $_POST['args']) and array_key_exists('passwd', $_POST['args']))
{
	/* Check login/password */
	if (!TNInterface\Lib\UserDB::verify($_POST['args']['login'], $_POST['args']['passwd']))
	{
		/* Send error */
		echo json_encode([
			                 'status'  => false,
			                 'message' => 'Authentication failed'
		                 ]);
		exit;
	}

	/* Init session */
	session_regenerate_id(true);

	/* Set $_SESSION data */
	$_SESSION = [];
	$_SESSION['user'] = $_POST['args']['login'];
	$_SESSION['token'] = TNInterface\Lib\Security::sessionGenerateToken($_POST['args']['login']);

	/* Send confirmation */
	echo json_encode([
		                 'status'  => true,
		                 'message' => 'Authentication success'
	                 ]);
	exit;
}

/* Disconnection request */
if (array_key_exists('disconnect', $_POST['args']) and (bool)$_POST['args']['disconnect'] === true)
{
	/* Wipe and destroy session */
	session_unset();
	session_destroy();

	/* Send confirmation */
	echo json_encode([
		                 'status'  => true,
		                 'message' => 'Disconnected'
	                 ]);
	exit;
}