<?php
/**
 * 3NInterface - Component loader AJAJ script @Server
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/


/* Check arguments */
if (!array_key_exists('component', $_POST) or !array_key_exists('token', $_POST))
{
	http_response_code(400);
	echo json_encode([
		                 'error'   => '400 Bad request',
		                 'message' => 'Argument(s) expected'
	                 ]);
	exit;
}

/* Check component (absolute path hack ?) */
if (strpos($_POST['component'], '/') !== false)
{
	http_response_code(409);
	echo json_encode([
		                 'error'   => '409 Conflict',
		                 'message' => 'Are you trying to hack me ?'
	                 ]);
	exit;
}

/* Check coffee pot */
if ($_POST['component'] === 'coffee')
{
	http_response_code(418);
	echo json_encode([
		                 'error'   => '418 I\'m a teapot',
		                 'message' => 'HTCPCP is not supported yet !'
	                 ]);
	exit;
}

/* Check session token */
if ($_POST['token'] !== $_SESSION['token'])
{
	http_response_code(403);
	echo json_encode([
		                 'error'   => '403 Forbidden',
		                 'message' => 'Bad token'
	                 ]);
	exit;
}

/* Absolute path of component */
$componentPath = dirname(__DIR__) . '/views/components/' . $_POST['component'] . '.html';

/* Check component */
if (!file_exists($componentPath))
{
	http_response_code(404);
	echo json_encode([
		                 'error'   => '404 Not found',
		                 'message' => 'Component not found'
	                 ]);
	exit;
}

/* Get component content */
$componentContent = file_get_contents($componentPath);
if ($componentPath === false)
{
	http_response_code(500);
	echo json_encode([
		                 'error'   => '500 Internal Server Error',
		                 'message' => 'Component error'
	                 ]);
	exit;
}

/* Send component content */
echo $componentContent;
exit;
