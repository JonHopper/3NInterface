<?php
/**
 * 3NInterface - Index page
 *
 * PHP 7.0
 *
 * @author     Manh-Tuong NGUYEN <nguyen_m@etna-alternance.net>
 * @version    1.0
 **/

require_once 'lib/Security.php';

session_start();

/* Check arguments (URL) */
if (array_key_exists('page', $_GET))
	$Page = $_GET['page'];
else
	$Page = 'index';

/* Check user connection */
if (!TNInterface\Lib\Security::sessionCheck() && $Page !== 'login')
{
	/* Check request method (if GET, request isn't action call) */
	if ($_SERVER['REQUEST_METHOD'] == 'GET')
	{
		header('Location: /login', true);
		exit;
	}

	/* Action call */
	header('Content-type: application/json', false);
	http_response_code(401);
	echo json_encode([
		                 'error'   => '401 Unauthorized',
		                 'message' => 'You must log in'
	                 ]);
	exit;
}

/* Action call (POST) */
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
	/* Check target */
	if (!array_key_exists('action', $_POST))
	{
		header('Content-type: application/json', false);
		http_response_code(400);
		echo json_encode([
			                 'error'   => '400 Bad request',
			                 'message' => 'You must specify a action'
		                 ]);
		exit;
	}

	/* Check action file */
	if (!file_exists(__DIR__ . '/actions/' . $_POST['action'] . '.php'))
	{
		header('Content-type: application/json', false);
		http_response_code(404);
		echo json_encode([
			                 'error'   => '404 Not found',
			                 'message' => 'Invalid action'
		                 ]);
		exit;
	}

	include_once __DIR__ . '/actions/' . $_POST['action'] . '.php';
	exit;
}

/* Page (GET) */
if ($_SERVER['REQUEST_METHOD'] === 'GET')
{
	if (!file_exists(__DIR__ . '/views/' . $Page . '.php'))
	{
		http_response_code(404);
		exit;
	}

	include_once __DIR__ . '/views/' . $Page . '.php';
}